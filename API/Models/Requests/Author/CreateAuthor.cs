using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication5.API.Models.Requests.Author
{
    public class CreateAuthor
    {
        [Required]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "User name could not be empty and greater than 255 symbols")]
        public string Name { get; set; }
    }
}