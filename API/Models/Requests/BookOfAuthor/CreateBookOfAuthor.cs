using System.ComponentModel.DataAnnotations;

namespace WebApplication5.API.Models.Requests.BookOfAuthor
{
    public class CreateBookOfAuthor
    {
        [Required]
        public int? AuthorId { get; set; }
        [Required]
        public int? BookId { get; set; }
    }
}