using System.ComponentModel.DataAnnotations;

namespace WebApplication5.API.Models.Requests.BookOfAuthor
{
    public class EditBookOfAuthor
    {
        [Required]
        public int? AuthorId { get; set; }
        [Required]
        public int? BookId { get; set; }
    }
}