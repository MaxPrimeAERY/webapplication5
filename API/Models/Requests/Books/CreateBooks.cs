using System.ComponentModel.DataAnnotations;

namespace WebApplication5.API.Models.Requests.Books
{
    public class CreateBooks
    {
        [Required]
        [StringLength(64, MinimumLength = 1, ErrorMessage = "User name could not be empty and greater than 64 symbols")]
        public string BookName { get; set; }
        [Required]
        [StringLength(64, MinimumLength = 1, ErrorMessage = "User name could not be empty and greater than 64 symbols")]
        public string Genre { get; set; }
        [Required]
        public int? Sections_Id { get; set; }
    }
}