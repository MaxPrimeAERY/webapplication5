using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication5.API.Models.Requests.CardOfReader
{
    public class CreateCardOfReader
    {
        [Required]
        public int? Reader_Id { get; set; }
        [Required]
        public int? Book_Id { get; set; }
        [Required]
        public DateTime? Get_Time { get; set; }
        [Required]
        public DateTime? Give_Time { get; set; }
        }
    
}