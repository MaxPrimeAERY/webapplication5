using System.ComponentModel.DataAnnotations;

namespace WebApplication5.API.Models.Requests.Reader
{
    public class EditReader
    {
        [Required]
        [StringLength(64, MinimumLength = 1, ErrorMessage = "User name could not be empty and greater than 64 symbols")]
        public string Name { get; set; }
    }
}