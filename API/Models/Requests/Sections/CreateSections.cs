using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication5.API.Models.Requests.Sections
{
    public class CreateSections
    {
        [Required]
        public List<string> SectionsNames { get; set; }
    }
}