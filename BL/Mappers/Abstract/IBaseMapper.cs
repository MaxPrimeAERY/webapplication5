using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication5.Entity.Entities;
using WebApplication5.ViewModels;

namespace WebApplication5.BL.Mappers.Abstract
{
    public abstract class IBaseMapper<T, K>
    {
        public abstract K MapAsModel(T data);

        public abstract T MapAsData(K model);

        public IEnumerable<K> MapAsModel(IEnumerable<T> datas)
        {
            return datas.Select(MapAsModel);
        }

    }
}