using WebApplication5.BL.Mappers.Abstract;
using WebApplication5.Entity.Entities;
using WebApplication5.ViewModels;

namespace WebApplication5.BL.Mappers.Impl
{
    public class DefaultAuthorMapper : IBaseMapper<Author2, AuthorViewModel>
    {
        public override AuthorViewModel MapAsModel(Author2 data)
        {
            return new AuthorViewModel()
            {
                Id = data.Id,
                Name = data.Name
            };
        }

        public override Author2 MapAsData(AuthorViewModel model)
        {
            return new Author2()
            {
                Id = model.Id,
                Name = model.Name
            };
        }
    }
}