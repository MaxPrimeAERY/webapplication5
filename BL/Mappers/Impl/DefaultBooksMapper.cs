using WebApplication5.BL.Mappers.Abstract;
using WebApplication5.Entity.Entities;
using WebApplication5.ViewModels;

namespace WebApplication5.BL.Mappers.Impl
{
    public class DefaultBooksMapper : IBaseMapper<BookOfAuthor, BooksViewModel>
    {
        public override BooksViewModel MapAsModel(BookOfAuthor data)
        {
            return new BooksViewModel()
            {
                Id = data.Id,
                AuthorId = data.AuthorId,
                BookId = data.BookId
            };
        }

        public override BookOfAuthor MapAsData(BooksViewModel model)
        {
            return new BookOfAuthor()
            {
                Id = model.Id,
                AuthorId = model.AuthorId,
                BookId = model.BookId
            };
        }
    }
}