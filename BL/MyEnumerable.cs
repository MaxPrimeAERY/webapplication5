using System.Collections;
using System.Collections.Generic;
using WebApplication5.Entity.Entities;

namespace WebApplication5.BL
{
    public class MyEnumerable : IEnumerable<BookOfAuthor>
    {
        private readonly IEnumerable<Author2> _authors;

        public MyEnumerable(IEnumerable<Author2> vs)
        {
            _authors = vs;
        }

        public IEnumerator<BookOfAuthor> GetEnumerator()
        {
            foreach(var author in _authors)
            {
                foreach(var book in author.Books)
                {
                    yield return book;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}