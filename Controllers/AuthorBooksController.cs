using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApplication5.BL;
using WebApplication5.BL.Mappers.Abstract;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities;
using WebApplication5.ViewModels;

namespace WebApplication5.Controllers
{
    [Route("api/authorbooks")]
    [ApiController]
    public class AuthorBooksController : ControllerBase
    {
        private readonly IAuthorRepository2 _repository;
        private readonly IBaseMapper<BookOfAuthor, BooksViewModel> _mapper;

        public AuthorBooksController(IAuthorRepository2 repo, IBaseMapper<BookOfAuthor, BooksViewModel> mapper)
        {
            _repository = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<BooksViewModel>> Get([FromBody] string namestart)
        {
            if (namestart == null) namestart = string.Empty;

            var authors = _repository.GetMatching((x) => x.Name.StartsWith(namestart));
            
            return _mapper.MapAsModel(new MyEnumerable(authors)).ToList();
        }
    }
}
