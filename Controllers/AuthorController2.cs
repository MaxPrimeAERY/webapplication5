using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApplication5.BL.Mappers.Abstract;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities;
using WebApplication5.ViewModels;

namespace WebApplication5.Controllers
{
    [Route("api/author")]
    [ApiController]
    public class AuthorController2 : ControllerBase
    {
        private readonly IAuthorRepository2 _repository;
        private readonly IBaseMapper<Author2, AuthorViewModel> _mapper;

        public AuthorController2(IAuthorRepository2 repo, IBaseMapper<Author2, AuthorViewModel> mapper)
        {
            _repository = repo;
            _mapper = mapper;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<AuthorViewModel>> Get()
        {
            return _mapper.MapAsModel(_repository.GetAll()).ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<AuthorViewModel> Get(int id)
        {
            var res = _repository.GetById(id);

            if (res == null) return NotFound();

            return _mapper.MapAsModel(res);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] AuthorViewModel value)
        {
            return _repository.Add(_mapper.MapAsData(value));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put([FromBody] AuthorViewModel value)
        {
            _repository.Update(_mapper.MapAsData(value));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _repository.RemoveById(id);
        }
    }
}