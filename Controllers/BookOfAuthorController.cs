using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication5.API.Models.Requests.BookOfAuthor;
using WebApplication5.BL.Mappers.Abstract;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities;
using WebApplication5.MvcExt;
using WebApplication5.Services;
using WebApplication5.ViewModels;

namespace WebApplication5.Controllers
{
    [Route("api/bookofauthor")]
    [ApiController]
    public class BookOfAuthorController : ControllerBase
    {
        private readonly IBaseRepository2<BookOfAuthor> _repository;
        private readonly IBaseMapper<BookOfAuthor, BooksViewModel> _mapper;

        public BookOfAuthorController(IBaseRepository2<BookOfAuthor> repo, IBaseMapper<BookOfAuthor, BooksViewModel> mapper)
        {
            _repository = repo;
            _mapper = mapper;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<BooksViewModel>> Get()
        {
            return _mapper.MapAsModel(_repository.GetAll()).ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<BooksViewModel> Get(int id)
        {
            var res = _repository.GetById(id);

            if (res == null) return NotFound();

            return _mapper.MapAsModel(res);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] BooksViewModel value)
        {
            return _repository.Add(_mapper.MapAsData(value));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put([FromBody] BooksViewModel value)
        {
            _repository.Update(_mapper.MapAsData(value));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _repository.RemoveById(id);
        }
    }
}