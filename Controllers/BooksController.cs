using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication5.API.Models.Requests.Books;
using WebApplication5.Entity.Entities;
using WebApplication5.MvcExt;
using WebApplication5.Services;

namespace WebApplication5.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBooksService _booksSvc;

        public BooksController(IBooksService booksSvc)
        {
            _booksSvc = booksSvc;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IList<Books>>> Get()
        {
            return new ActionResult<IList<Books>>(await _booksSvc.GetAll());
        }
        
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Books>> Get(int id)
        {
            var books = await _booksSvc.GetById(id);
            if (books == null)
            {
                return new NotFoundObjectResult(null);
            }

            return books;
        }
        
        [HttpGet]
        [ExactQueryParam("sectionsId")]
        public async Task<ActionResult<IEnumerable<Books>>> GetBySectionsId(int sectionsId)
        {
            return new ActionResult<IEnumerable<Books>>(await _booksSvc.GetBySectionsId(sectionsId));
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<Books>> Post([FromBody] CreateBooks value)
        {
            return await _booksSvc.Create(value);
        }

        // PUT api/books/5                                             ?????????????????????????????????
        [HttpPut("{id}")]
        public async Task<ActionResult<Books>> Put(int id, EditBooks value)
        {
            await _booksSvc.Update(id, value.BookName, value.Genre, value.Sections_Id);
            return await _booksSvc.GetById(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _booksSvc.Delete(id);
        }
    }
}