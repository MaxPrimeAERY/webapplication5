using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication5.API.Models.Requests.Sections;
using WebApplication5.Entity.Entities;
using WebApplication5.Services;

namespace WebApplication5.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SectionsController : ControllerBase
    {
        private readonly ISectionsService _sectionsSvc;

        public SectionsController(ISectionsService sectionsSvc)
        {
            _sectionsSvc = sectionsSvc;
        }
        
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IList<Sections>>> Get()
        {
            return new ActionResult<IList<Sections>>(await _sectionsSvc.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sections>> Get(int id)
        {
            var message = await _sectionsSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }
            return (message);
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<Sections>> Post([FromBody] CreateSections value)
        {
            return await _sectionsSvc.Create(value);
        }

        // PUT api/sections/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Sections>> Put(int id, [FromBody] EditSections value)
        {
            
            await _sectionsSvc.Update(id, value.SectionsNames);
            return await _sectionsSvc.GetById(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _sectionsSvc.Delete(id);
        }
    }
}