using System;
using System.Collections.Generic;
using WebApplication5.Entity.Entities;

namespace WebApplication5.DAL.Abstract
{
    public interface IAuthorRepository2 : IBaseRepository2<Author2>
    {
        IEnumerable<Author2> GetMatching(Func<Author2, bool> predicate);
    }
}
