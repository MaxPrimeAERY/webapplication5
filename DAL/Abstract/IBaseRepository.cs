using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication5.Entity.Entities;
using WebApplication5.Entity.Entities.Abstract;

namespace WebApplication5.DAL.Abstract
{
    public interface IBaseRepository<TKey, TEntity> where TEntity : IBaseEntity<TKey>
    {
        Task<TKey> Insert(TEntity entity);
        Task<bool> Update(TEntity entity);
        Task<TKey> Upsert(TEntity entity);

        Task<int> GetCount();

        Task<TEntity> GetById(TKey id);
        Task<bool> Delete(TKey id);

        Task<IList<TEntity>> GetAll();
    }
}