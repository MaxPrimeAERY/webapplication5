using System.Collections.Generic;

namespace WebApplication5.DAL.Abstract
{
    public interface IBaseRepository2<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        int Add(T value);
        void Update(T value);
        void RemoveById(int id);
    }
}