using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication5.Entity.Entities;

namespace WebApplication5.DAL.Abstract
{
    public interface IBooksRepository : IBaseRepository<int, Books>
    {
        Task<IList<Books>> GetBySectionsId(int sectId);
    }
}