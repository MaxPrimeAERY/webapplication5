using WebApplication5.Entity.Entities;

namespace WebApplication5.DAL.Abstract
{
    public interface IReaderRepository : IBaseRepository<int, Reader>
    {
        
    }
}