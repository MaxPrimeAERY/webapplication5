using WebApplication5.Entity.Entities;

namespace WebApplication5.DAL.Abstract
{
    public interface ISectionsRepository : IBaseRepository<int, Sections>
    {
        
    }
}