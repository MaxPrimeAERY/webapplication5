using System;
using System.Threading.Tasks;

namespace WebApplication5.DAL.Abstract
{
    public interface ITransactionManager : IDisposable
    {
        /// <summary>
        /// Returned disposable will auto close TX (rollback if not commited)
        /// </summary>
        /// <returns></returns>
        Task<IDisposable> BeginAsync();
        Task CommitAsync();
        Task RollBackAsync();
    }
}