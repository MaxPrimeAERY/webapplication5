using System;
using System.Reflection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplication5.Entity.Entities;

namespace WebApplication5.DAL.EntityFramework
{
    public class AppContext : IdentityDbContext
    {

        public AppContext(DbContextOptions<AppContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        //public virtual DbSet<Author> Author { get; set; }
        //public virtual DbSet<BookOfAuthor> BookOfAuthor { get; set; }
        public virtual DbSet<Books> Books { get; set; }
        public virtual DbSet<CardOfReader> CardOfReader { get; set; }
        public virtual DbSet<Reader> Reader { get; set; }
        public virtual DbSet<Sections> Sections { get; set; }
        
        public virtual DbSet<Author2> Author { get; set; }
        public virtual DbSet<BookOfAuthor> BookOfAuthor { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Author2>().ToTable("Author");
            modelBuilder.Entity<BookOfAuthor>().ToTable("BookOfAuthor");

        }
        
    }
}