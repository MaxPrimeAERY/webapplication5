using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities.Abstract;

namespace WebApplication5.DAL.EntityFramework.Repository
{
    public class GlobalRepository<TKey, TEntity> : 
        IBaseRepository<TKey, TEntity> where TEntity : class, IBaseEntity<TKey>
    {
        public GlobalRepository(AppContext context)
        {
            Context = context;
        }

        public AppContext Context { get; }

        public DbSet<TEntity> DbSet => Context.Set<TEntity>();

        
        protected async Task<int> SaveAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public virtual async Task<TKey> Insert(TEntity entity)
        {
            var item = await Context.Set<TEntity>().AddAsync(entity);
            await SaveAsync();
            return item.Entity.Id;
        }

        public virtual async Task<bool> Update(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            await SaveAsync();
            return true;
        }

        public virtual async Task<TKey> Upsert(TEntity entity)
        {
            if (Object.Equals((IBaseEntity<TKey>) entity.Id, default(TKey)))
                return await Insert(entity);
            else
            {
                if (await Update(entity))
                    return entity.Id;
                else
                    return default(TKey);
            }
        }

        public virtual async Task<int> GetCount()
        {
            return await Context.Set<TEntity>().CountAsync();
        }

        public virtual async Task<TEntity> GetById(TKey id)
        {
            return await Context.Set<TEntity>()
                .FindAsync(id);
        }

        public virtual async Task<bool> Delete(TKey id)
        {
            TEntity element =
                await GetById(id);
            TEntity result = Context.Set<TEntity>()
                .Remove(element).Entity;
            return 0 != await SaveAsync();
        }

        public virtual async Task<IList<TEntity>> GetAll()
        {
            return await Context.Set<TEntity>().ToListAsync();
        }
    }
}