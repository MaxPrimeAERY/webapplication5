using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities.Abstract;

namespace WebApplication5.DAL.EntityFramework.Repository
{
    public class GlobalRepository2<T> : IBaseRepository2<T> where T : class, IBaseEntity2
    {
        private readonly AppContext _context;

        protected DbSet<T> Set { get { return _context.Set<T>(); } }

        public GlobalRepository2(AppContext cont) { _context = cont; }

        public IEnumerable<T> GetAll()
        {
            return Set.AsEnumerable();
        }

        public T GetById(int id)
        {
            return Set.FirstOrDefault(x => x.Id == id);
        }

        public int Add(T value)
        {
            Set.Add(value);
            return _context.SaveChanges();
        }

        public void Update(T value)
        {
            Set.Update(value);
            _context.SaveChanges();
        }

        public void RemoveById(int id)
        {
            Set.Remove(GetById(id));
            _context.SaveChanges();
        }
    }
}
