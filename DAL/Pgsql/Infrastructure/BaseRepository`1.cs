﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication5.DAL.Abstract;
using JetBrains.Annotations;

namespace WebApplication5.DAL.Pgsql.Infrastructure
{
    internal abstract class BaseRepository<TEntity>
    {
        private readonly DbConnection _connection;
        private readonly ITransactionManager _transactionManager;

        protected BaseRepository(DbConnection connection, ITransactionManager transactionManager)
        {
            _connection = connection;
            _transactionManager = transactionManager;
        }

        protected async Task<DbConnection> GetConnectionAsync()
        {
            await ConnectionHelper.CheckConnectionAsync(_connection);
            return _connection;
        }

        protected async Task<DbCommand> CreateCommand(string sql)
        {
            var connection = await GetConnectionAsync();
            var command = connection.CreateCommand();
            command.CommandText = sql;
            command.Transaction = ((PgSqlTransactionManager)_transactionManager).CurrentTransaction;
            
            return command;
        }

        protected async Task<T> ExecuteScalar<T>(string sql, IDictionary<string, object> parameters = null)
        {
            using (DbCommand command = await CreateCommand(sql))
            {
                FillParameters(parameters, command);

                return (T)command.ExecuteScalar();
            }
        }

        protected async Task<int> ExecuteNonQuery(string sql, IDictionary<string, object> parameters=null)
        {
            using (DbCommand command = await CreateCommand(sql))
            {
                FillParameters(parameters, command);

                return command.ExecuteNonQuery();
            }
        }

        protected async Task<T> ExecuteSingleRowSelect<T>(
            string sql,
            Func<DbDataReader,T> rowMapping, 
            IDictionary<string, object> parameters = null
            )
        {
            using (DbCommand command = await CreateCommand(sql))
            {
                FillParameters(parameters, command);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return rowMapping(reader);
                    }
                    else
                    {
                        return default(T);
                    }
                }
            }
        }


        protected async Task<IList<T>> ExecuteSelect<T>(
            string sql,
            Func<DbDataReader, T> rowMapping,
            IDictionary<string, object> parameters = null
            )
        {
            using (DbCommand command = await CreateCommand(sql))
            {
                FillParameters(parameters, command);

                using (var reader = command.ExecuteReader())
                {
                    List<T> list = new List<T>(1);
                    while (reader.Read())
                    {
                        list.Add(rowMapping(reader));
                    }

                    return list;
                }
            }
        }


        protected Task<TEntity> ExecuteSingleRowSelect(string sql, SqlParameters sqlParameters = null)
        {
            return ExecuteSingleRowSelect(sql, DefaultRowMapping, sqlParameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="sqlParameters">language=SQL</param>
        /// <returns></returns>
        protected Task<IList<TEntity>> ExecuteSelect(string sql, SqlParameters sqlParameters = null)
        {
            return ExecuteSelect(sql, DefaultRowMapping, sqlParameters);
        }

        private static void FillParameters(IDictionary<string, object> parameters, DbCommand command)
        {
            if (parameters != null)
                foreach (var parameter in parameters)
                {
                    var param = command.CreateParameter();
                    param.ParameterName = parameter.Key;
                    param.Value = parameter.Value ?? DBNull.Value;
                    command.Parameters.Add(param);
                }
        }

        protected abstract TEntity DefaultRowMapping(DbDataReader reader);

    }
}