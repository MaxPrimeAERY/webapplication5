using System;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities.Abstract;

namespace WebApplication5.DAL.Pgsql.Infrastructure
{
    internal abstract class BaseRepository<TKey, TEntity>:BaseRepository<TEntity>
        where TEntity : IBaseEntity<TKey>
    {
        protected BaseRepository(DbConnection connection, ITransactionManager transactionManager)
            : base(connection, transactionManager)
        {
        }

        public async Task<TKey> Upsert(TEntity entity)
        {
            if (Object.Equals(entity.Id, default(TKey)))
                return await Insert(entity);
            else
            {
                if (await Update(entity))
                    return entity.Id;
                else
                    return default(TKey);
            }
        }

        public abstract Task<TKey> Insert(TEntity entity);
        public abstract Task<bool> Update(TEntity entity);

    }
}