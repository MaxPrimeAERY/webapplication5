using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Npgsql;

namespace WebApplication5.DAL.Pgsql.Infrastructure
{
    public static class ConnectionHelper
    {
        internal static async Task CheckConnectionAsync(DbConnection connection)
        {
            var connectionState = connection.State;
            if (connectionState != ConnectionState.Open)
            {
                if (connectionState == ConnectionState.Closed)
                {
                    await connection.OpenAsync();
                }
                else
                {
                    throw new InvalidOperationException("Connection is in state: " + connectionState);
                }
            }
        }
    }
}