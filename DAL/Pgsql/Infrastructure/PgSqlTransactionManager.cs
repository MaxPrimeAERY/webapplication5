﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using Npgsql;
using WebApplication5.DAL.Abstract;

namespace WebApplication5.DAL.Pgsql.Infrastructure
{
    internal class PgSqlTransactionManager : ITransactionManager
    {
        private readonly NpgsqlConnection _connection;
        private TxHandle _transaction;

        public PgSqlTransactionManager(NpgsqlConnection connection)
        {
            _connection = connection;
        }

        internal DbTransaction CurrentTransaction => _transaction?.Transaction;

        //no destructor, as connection close would roll back all uncommitted TXs
        public void Dispose()
        {
            _transaction?.Dispose();
            _transaction = null;
        }

        public async Task<IDisposable> BeginAsync()
        {
            if (_transaction != null)
            {
                //TODO add nested TX, aka. checkpoints support
                throw new InvalidOperationException("Nested transactions aren't supported");
                #pragma warning disable 162
                _transaction.Dispose();
                #pragma warning restore 162
            }

            await ConnectionHelper.CheckConnectionAsync(_connection);

            _transaction = new TxHandle(_connection.BeginTransaction( /*IsolationLevel.ReadCommitted*/), this);
            return _transaction;
        }

        public async Task CommitAsync()
        {
            if (_transaction?.Transaction != null)
            {
                await _transaction.Transaction.CommitAsync();
            }

            _transaction = null;
        }

        public async Task RollBackAsync()
        {
            if (_transaction?.Transaction != null)
            {
                await _transaction.Transaction.RollbackAsync();
            }

            _transaction = null;
        }

        class TxHandle : IDisposable
        {
            private readonly PgSqlTransactionManager _manager;
            internal readonly NpgsqlTransaction Transaction;

            public TxHandle(NpgsqlTransaction transaction, PgSqlTransactionManager manager)
            {
                Transaction = transaction;
                _manager = manager;
            }

            public void Dispose()
            {
                Transaction?.Dispose();
                _manager._transaction = null;
            }
        }
    }
}