using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication5.DAL.Abstract;
using WebApplication5.DAL.EntityFramework.Repository;
using WebApplication5.Entity.Entities;
using AppContext = WebApplication5.DAL.EntityFramework.AppContext;

namespace WebApplication5.DAL.Pgsql.Repository
{
    public class AuthorRepository2 : GlobalRepository2<Author2>, IAuthorRepository2
    {
        public AuthorRepository2(AppContext cont) : base(cont) { }

        public IEnumerable<Author2> GetMatching(Func<Author2, bool> predicate)
        {
            return Set.Where(predicate);
        }

    }
}