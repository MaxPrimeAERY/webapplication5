using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication5.DAL.Abstract;
using WebApplication5.DAL.EntityFramework.Repository;
using WebApplication5.DAL.Pgsql.Infrastructure;
using WebApplication5.Entity.Entities;
using AppContext = WebApplication5.DAL.EntityFramework.AppContext;

namespace WebApplication5.DAL.Pgsql.Repository
{
    internal class BooksRepository : GlobalRepository<int, Books>, IBooksRepository
    {
        public BooksRepository(AppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<IList<Books>> GetBySectionsId(int sectionsId)
        {
            return await Context.Set<Books>()
                .Where(p => p.Sections_Id == sectionsId)
                .ToListAsync();
        }
        
//        public async Task<IList<Books>> GetAll()
//        {
//            return await Context.Set<Books>().ToListAsync();
//        }
//        
//        public async Task<int> Insert(Books entity)
//        {
//            var item = await Context.Set<Books>().AddAsync(entity);
//            await SaveAsync();
//            return item.Entity.Id;
//        }
//
//        public async Task<bool> Update(Books entity)
//        {
//            Context.Entry(entity).State = EntityState.Modified;
//            await SaveAsync();
//            return true;
//        }
//
//        public async Task<int> Upsert(Books entity)
//        {
//            if (Object.Equals(entity.Id, default(int)))
//                return await Insert(entity);
//            else
//            {
//                if (await Update(entity))
//                    return entity.Id;
//                else
//                    return default(int);
//            }
//        }
//
//        public async Task<int> GetCount()
//        {
//            return await Context.Set<Books>().CountAsync();
//        }
//
//        public async Task<Books> GetById(int id)
//        {
//            return await Context.Set<Books>()
//                .FindAsync(id);
//        }
//
//        public async Task<bool> Delete(int id)
//        {
//            Books element =
//                await GetById(id);
//            Books result = Context.Set<Books>()
//                .Remove(element).Entity;
//            return 0 != await SaveAsync();
//        }

    }
}