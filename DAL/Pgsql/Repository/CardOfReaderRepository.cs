using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication5.DAL.Abstract;
using WebApplication5.DAL.EntityFramework.Repository;
using WebApplication5.DAL.Pgsql.Infrastructure;
using WebApplication5.Entity.Entities;
using AppContext = WebApplication5.DAL.EntityFramework.AppContext;

namespace WebApplication5.DAL.Pgsql.Repository
{
    internal class CardOfReaderRepository : GlobalRepository<int, CardOfReader>, ICardOfReaderRepository
    {
        public CardOfReaderRepository(AppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<IList<CardOfReader>> GetByReaderId(int readerId)
        {
            return await Context.Set<CardOfReader>()
                .Where(p => p.Reader_Id == readerId)
                .ToListAsync();
        }

        public async Task<IList<CardOfReader>> GetByBookId(int bookId)
        {
            return await Context.Set<CardOfReader>()
                .Where(p => p.Book_Id == bookId)
                .ToListAsync();
        }
        
//        public async Task<IList<CardOfReader>> GetAll()
//        {
//            return await Context.Set<CardOfReader>().ToListAsync();
//        }
//        
//        public async Task<int> Insert(CardOfReader entity)
//        {
//            var item = await Context.Set<CardOfReader>().AddAsync(entity);
//            await SaveAsync();
//            return item.Entity.Id;
//        }
//
//        public async Task<bool> Update(CardOfReader entity)
//        {
//            Context.Entry(entity).State = EntityState.Modified;
//            await SaveAsync();
//            return true;
//        }
//
//        public async Task<int> Upsert(CardOfReader entity)
//        {
//            if (Object.Equals(entity.Id, default(int)))
//                return await Insert(entity);
//            else
//            {
//                if (await Update(entity))
//                    return entity.Id;
//                else
//                    return default(int);
//            }
//        }
//
//        public async Task<int> GetCount()
//        {
//            return await Context.Set<CardOfReader>().CountAsync();
//        }
//
//        public async Task<CardOfReader> GetById(int id)
//        {
//            return await Context.Set<CardOfReader>()
//                .FindAsync(id);
//        }
//
//        public async Task<bool> Delete(int id)
//        {
//            CardOfReader element =
//                await GetById(id);
//            CardOfReader result = Context.Set<CardOfReader>()
//                .Remove(element).Entity;
//            return 0 != await SaveAsync();
//        }

    }
}