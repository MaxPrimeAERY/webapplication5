using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication5.DAL.Abstract;
using WebApplication5.DAL.EntityFramework.Repository;
using WebApplication5.DAL.Pgsql.Infrastructure;
using WebApplication5.Entity.Entities;
using AppContext = WebApplication5.DAL.EntityFramework.AppContext;

namespace WebApplication5.DAL.Pgsql.Repository
{
    internal class ReaderRepository : GlobalRepository<int, Reader>, IReaderRepository
    {
        public ReaderRepository(AppContext ctx) : base(ctx)
        {
            
        }
        
//        public async Task<IList<Reader>> GetAll()
//        {
//            return await Context.Set<Reader>().ToListAsync();
//        }
//        
//        public async Task<int> Insert(Reader entity)
//        {
//            var item = await Context.Set<Reader>().AddAsync(entity);
//            await SaveAsync();
//            return item.Entity.Id;
//        }
//
//        public async Task<bool> Update(Reader entity)
//        {
//            Context.Entry(entity).State = EntityState.Modified;
//            await SaveAsync();
//            return true;
//        }
//
//        public async Task<int> Upsert(Reader entity)
//        {
//            if (Object.Equals(entity.Id, default(int)))
//                return await Insert(entity);
//            else
//            {
//                if (await Update(entity))
//                    return entity.Id;
//                else
//                    return default(int);
//            }
//        }
//
//        public async Task<int> GetCount()
//        {
//            return await Context.Set<Reader>().CountAsync();
//        }
//
//        public async Task<Reader> GetById(int id)
//        {
//            return await Context.Set<Reader>()
//                .FindAsync(id);
//        }
//
//        public async Task<bool> Delete(int id)
//        {
//            Reader element =
//                await GetById(id);
//            Reader result = Context.Set<Reader>()
//                .Remove(element).Entity;
//            return 0 != await SaveAsync();
//        }

    }
}