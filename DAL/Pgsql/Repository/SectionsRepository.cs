using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication5.DAL.Abstract;
using WebApplication5.DAL.EntityFramework.Repository;
using WebApplication5.DAL.Pgsql.Infrastructure;
using WebApplication5.Entity.Entities;
using AppContext = WebApplication5.DAL.EntityFramework.AppContext;

namespace WebApplication5.DAL.Pgsql.Repository
{
    internal class SectionsRepository : GlobalRepository<int, Sections>, ISectionsRepository
    {
        public SectionsRepository(AppContext ctx) : base(ctx)
        {
            
        }
        
//        public async Task<IList<Sections>> GetAll()
//        {
//            return await Context.Set<Sections>().ToListAsync();
//        }
//        
//        public async Task<int> Insert(Sections entity)
//        {
//            var item = await Context.Set<Sections>().AddAsync(entity);
//            await SaveAsync();
//            return item.Entity.Id;
//        }
//
//        public async Task<bool> Update(Sections entity)
//        {
//            Context.Entry(entity).State = EntityState.Modified;
//            await SaveAsync();
//            return true;
//        }
//
//        public async Task<int> Upsert(Sections entity)
//        {
//            if (Object.Equals(entity.Id, default(int)))
//                return await Insert(entity);
//            else
//            {
//                if (await Update(entity))
//                    return entity.Id;
//                else
//                    return default(int);
//            }
//        }
//
//        public async Task<int> GetCount()
//        {
//            return await Context.Set<Sections>().CountAsync();
//        }
//
//        public async Task<Sections> GetById(int id)
//        {
//            return await Context.Set<Sections>()
//                .FindAsync(id);
//        }
//
//        public async Task<bool> Delete(int id)
//        {
//            Sections element =
//                await GetById(id);
//            Sections result = Context.Set<Sections>()
//                .Remove(element).Entity;
//            return 0 != await SaveAsync();
//        }

    }
}