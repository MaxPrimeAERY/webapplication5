namespace WebApplication5.Entity.Entities.Abstract
{
    public interface IBaseEntity<TKey>
    {
        TKey Id { get; }
    }
}