using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApplication5.Entity.Entities.Abstract;

namespace WebApplication5.Entity.Entities
{
    [Table("Author")]
    public class Author2 : IBaseEntity2
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        
        public virtual ICollection<BookOfAuthor> Books { get; set; }
    }
}