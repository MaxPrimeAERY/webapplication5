using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApplication5.Entity.Entities.Abstract;

namespace WebApplication5.Entity.Entities
{
    [Table("Books")]
    public class Books2 : IBaseEntity2
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("bookname")] public string BookName { get; set; }
        [Column("genre")] public string Genre { get; set; }

        [ForeignKey("Sections")]
        [Column("sections_id")]
        public int Sections_Id { get; set; }

        //public virtual Author2 Author { get; set; }
    }
}