using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApplication5.Entity.Entities.Abstract;

namespace WebApplication5.Entity.Entities
{
    public class CardOfReader:IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        //[ForeignKey("Reader")]
        [Column("reader_id")]
        public int Reader_Id { get; set; }
        //[ForeignKey("Books")]
        [Column("book_id")]
        public int Book_Id { get; set; }
        [Column("get_time")]
        public DateTime? Get_Time { get; set; }
        [Column("give_time")]
        public DateTime? Give_Time { get; set; }
    }
}