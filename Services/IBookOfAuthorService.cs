using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication5.API.Models.Requests.BookOfAuthor;
using WebApplication5.Entity.Entities;

namespace WebApplication5.Services
{
    public interface IBookOfAuthorService
    {
        Task<BookOfAuthor> GetById(int id);
        Task<IList<BookOfAuthor>> GetAll();
        Task<IList<BookOfAuthor>> GetByAuthorId(int id);
        Task<IList<BookOfAuthor>> GetByBookId(int id);
        Task Delete(int id);
        Task Update(int bookOfAuthorId, int? valueBooksId, int? valueBookId);
        Task<BookOfAuthor> Create(CreateBookOfAuthor value);
    }
}