using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication5.API.Models.Requests.Books;
using WebApplication5.Entity.Entities;

namespace WebApplication5.Services
{
    public interface IBooksService
    {
        Task<Books> GetById(int id);
        Task<IList<Books>> GetAll();
        Task<IList<Books>> GetBySectionsId(int id);
        Task Delete(int id);
        Task Update(int bookId, string bookName, string genre, int sectionsId);
        Task<Books> Create(CreateBooks value);
    }
}