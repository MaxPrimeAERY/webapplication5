using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication5.API.Models.Requests.CardOfReader;
using WebApplication5.Entity.Entities;

namespace WebApplication5.Services
{
    public interface ICardOfReaderService
    {
        Task<CardOfReader> GetById(int id);
        Task<IList<CardOfReader>> GetAll();
        Task<IList<CardOfReader>> GetByReaderId(int id);
        Task<IList<CardOfReader>> GetByBookId(int id);
        Task Delete(int id);
        Task Update(int corId, DateTime? valueGiveTimeOffset);
        Task<CardOfReader> Create(CreateCardOfReader value);
    }
}