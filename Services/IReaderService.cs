using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication5.API.Models.Requests.Reader;
using WebApplication5.Entity.Entities;

namespace WebApplication5.Services
{
    public interface IReaderService
    {
        Task<Reader> GetById(int id);
        Task<IList<Reader>> GetAll();
        Task Delete(int id);
        Task Update(int readerId, string valueText);
        Task<Reader> Create(CreateReader value);
    }
}