using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication5.API.Models.Requests.Books;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities;

namespace WebApplication5.Services.Impl
{
    public class BooksService : IBooksService
    {
        private IBooksRepository _booksRepo;

        public BooksService(IBooksRepository booksRepo)
        {
            _booksRepo = booksRepo;

        }
        public Task<Books> GetById(int id)
        {
            return _booksRepo.GetById(id);
        }

        public Task<IList<Books>> GetAll()
        {
            return _booksRepo.GetAll();
        }
        
        public Task<IList<Books>> GetBySectionsId(int id)
        {
            return _booksRepo.GetBySectionsId(id);
        }

        public async Task Delete(int id)
        {
            var res = await _booksRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Books> Create(CreateBooks value)
        {
            var book = new Books()
            {
                BookName = value.BookName,
                Genre = value.Genre,
                Sections_Id = value.Sections_Id??0
            };

            var id = await _booksRepo.Insert(book);
            book.Id = id;
            return book;
        }

        public async Task Update(int booksId, string bookName, string genre, int sections_Id)
        {
            var books = await _booksRepo.GetById(booksId);
            books.BookName = bookName;
            books.Genre = genre;
            books.Sections_Id = sections_Id;
            await _booksRepo.Update(books);
        }
    }
}