using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication5.API.Models.Requests.CardOfReader;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities;

namespace WebApplication5.Services.Impl
{
    public class CardOfReaderService : ICardOfReaderService
    {
        private ICardOfReaderRepository _cardOfReaderRepo;

        public CardOfReaderService(ICardOfReaderRepository cardOfReaderRepo)
        {
            _cardOfReaderRepo = cardOfReaderRepo;

        }
        public Task<CardOfReader> GetById(int id)
        {
            return _cardOfReaderRepo.GetById(id);
        }

        public Task<IList<CardOfReader>> GetAll()
        {
            return _cardOfReaderRepo.GetAll();
        }
        
        public Task<IList<CardOfReader>> GetByReaderId(int id)
        {
            return _cardOfReaderRepo.GetByReaderId(id);
        }
        
        public Task<IList<CardOfReader>> GetByBookId(int id)
        {
            return _cardOfReaderRepo.GetByBookId(id);
        }

        public async Task Delete(int id)
        {
            var res = await _cardOfReaderRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<CardOfReader> Create(CreateCardOfReader value)
        {
            var cardOfReader = new CardOfReader()
            {
                Reader_Id = value.Reader_Id??0,
                Book_Id = value.Book_Id??0,
                Get_Time = DateTime.Now
            };

            var id = await _cardOfReaderRepo.Insert(cardOfReader);
            cardOfReader.Id = id;
            return cardOfReader;
        }

        public async Task Update(int cardId, DateTime? valueGiveTime)
        {
            var cardOfReader = await _cardOfReaderRepo.GetById(cardId);
            cardOfReader.Give_Time = valueGiveTime;
            await _cardOfReaderRepo.Update(cardOfReader);
        }
    }
}