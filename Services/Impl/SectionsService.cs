using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication5.API.Models.Requests.Sections;
using WebApplication5.DAL.Abstract;
using WebApplication5.Entity.Entities;

namespace WebApplication5.Services.Impl
{
    public class SectionsService : ISectionsService
    {
        private ISectionsRepository _sectionsRepo;

        public SectionsService(ISectionsRepository sectionsRepo)
        {
            _sectionsRepo = sectionsRepo;

        }
        public Task<Sections> GetById(int id)
        {
            return _sectionsRepo.GetById(id);
        }

        public Task<IList<Sections>> GetAll()
        {
            return _sectionsRepo.GetAll();
        }

        public async Task Delete(int id)
        {
            var res = await _sectionsRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Sections> Create(CreateSections value)
        {
            var sections = new Sections()
            {
                SectionsNames = value.SectionsNames
            };

            var id = await _sectionsRepo.Insert(sections);
            sections.Id = id;
            return sections;
        }

        public async Task Update(int sectId, List<string> valueName)
        {
            var sections = await _sectionsRepo.GetById(sectId);
            sections.SectionsNames = valueName;
            await _sectionsRepo.Update(sections);
        }
    }
}