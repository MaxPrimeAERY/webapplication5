﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebApplication5.BL.Mappers.Abstract;
using WebApplication5.BL.Mappers.Impl;
using WebApplication5.DAL.EntityFramework;
using WebApplication5.Services.Impl;
using WebApplication5.DAL.Pgsql;
using WebApplication5.Entity.Entities;
using WebApplication5.Services;
using WebApplication5.ViewModels;
using AppContext = WebApplication5.DAL.EntityFramework.AppContext;

namespace WebApplication5
{
    
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // https://www.npgsql.org/doc/connection-string-parameters.html
            var connectionString = Configuration.GetConnectionString("Default");
//            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
//
//            // Configure JwtIssuerOptions
//            services.Configure<JwtIssuerOptions>(options =>
//            {
//                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
//                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
//                //options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
//            });
            
            services.UsePgSqlEF(connectionString);
            
            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    //string keyyee = Encoding.UTF8.GetBytes(jwtAppSettingOptions[nameof(JwtIssuerOptions.Key)]).ToString();
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuerOptions:Issuer"],
                        ValidAudience = Configuration["JwtIssuerOptions:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtIssuerOptions:Key"])),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });

            services.AddMvc()
                .AddJsonOptions(options => 
                    options.SerializerSettings.ReferenceLoopHandling = 
                        Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //services.AddTransient<IAuthorService, AuthorService>();
            //services.AddTransient<IBookOfAuthorService, BookOfAuthorService>();
            services.AddTransient<IBooksService, BooksService>();
            services.AddTransient<ICardOfReaderService, CardOfReaderService>();
            services.AddTransient<IReaderService, ReaderService>();
            services.AddTransient<ISectionsService, SectionsService>();
            
            
            services.AddSingleton<IBaseMapper<BookOfAuthor, BooksViewModel>, DefaultBooksMapper>();
            services.AddSingleton<IBaseMapper<Author2, AuthorViewModel>, DefaultAuthorMapper>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)//
        {
//            app.UseStaticFiles();

            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new
                    {
                        controller = "Home",
                        action = "Index"
                    });
            });
            
            //dbContext.Database.EnsureCreated();
        }
    }
}