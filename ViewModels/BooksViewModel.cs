using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication5.ViewModels
{
    public class BooksViewModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [ForeignKey("Author")]
        [Column("author_id")]
        public int? AuthorId { get; set; }
        [ForeignKey("Books")]
        [Column("book_id")]
        public int? BookId { get; set; }
    }
}